import React from 'react';
import {Router, Switch} from 'dva/router';
import {IntlProvider} from 'react-intl';
import {LocaleProvider} from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import {LoginPage, ProfilePage, AdminQualityControlPage} from './routes';
import {AppRoute} from './components/AppRoute';
import {GuestLayout} from './layouts/GuestLayout';
import {UserLayout} from './layouts/UserLayout';


export default ({history}) => (
    <IntlProvider locale="en">
        <LocaleProvider locale={enUS}>
            <Router history={history}>
                <Switch>
                    <AppRoute path="/admin/quality-control" layout={UserLayout} component={AdminQualityControlPage} exact/>
                    <AppRoute path="/profile" layout={UserLayout} component={ProfilePage}/>
                    <AppRoute path="/login/restore/:email" layout={GuestLayout} component={LoginPage} exact/>
                    <AppRoute path="/login" layout={GuestLayout} component={LoginPage}/>
                    <AppRoute path="/forgot" layout={GuestLayout} component={LoginPage}/>
                </Switch>
            </Router>
        </LocaleProvider>
    </IntlProvider>
);
