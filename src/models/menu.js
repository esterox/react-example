import {getWatcher} from 'utils/watcher';
import {load} from '../services/menu';
import {get} from 'lodash';

export const Menu = {
    namespace: 'menu',
    state: {
        user: {},
        data: [],
    },
    reducers: {
        SET_STATE: (state, action) => ({...state, ...action.payload}),
    },
    effects: {
        * ENSURE({payload}, saga) {
            const user = yield saga.select(({user}) => ({...user}));
            const lastUserState = yield saga.select(({menu}) => JSON.stringify(menu.user));
            const userState = JSON.stringify(user);

            // apply permissions for list
            const applyPermissions = (list) => list.reduce((array, entry) => {
                return array.concat(predicate(entry) ? {
                    ...entry,
                    children: entry.children ? applyPermissions(entry.children) : null,
                } : []);
            }, []);

            // predicate for entry filtering
            const predicate = ({access = []}) => access.every((path) => {
                const [property, entry] = path.split(':');
                return get(user, `customer.${property}`, []).includes(entry);
            });

            // reload menu if user state is changed
            if (userState !== lastUserState) {
                let data = yield saga.call(load, user);

                // Apply permissions for menu
                data = applyPermissions(data);

                // Save state
                yield saga.put({
                    type: 'SET_STATE',
                    payload: {
                        data,
                        user,
                    },
                });
            }
        },
        authorizedWatcher: getWatcher(() => true, function* ({put, take}) {
            yield put({
                type: 'ENSURE',
            });
        }),
    },
}; 