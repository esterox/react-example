import {
    getAdminsList,
    getAdmin,
    createAdmin,
    updateAdmin,
    deleteAdmin,
} from 'services/admins';
import {routerRedux} from 'dva/router';
import {getWatcher} from '../utils/watcher';

export const Admins = {
    namespace: 'admins',
    state: {
        list: [],
        isLoading: true,
        entry: [],
        isLoadingEntry: false,
        isUpdating: false,
    },
    reducers: {
        SET_STATE: (state, action) => ({...state, ...action.payload}),
    },
    effects: {
        * GET_ADMINS({
                         sort = 'id',
                         perPage = 1000,
                         currentPage = '',
                         filter = {isAdmin: true},
                     }, {
                         call, put,
                     }) {
            yield put({
                type: 'SET_STATE',
                payload: {
                    isLoading: true,
                },
            });
            const list = yield call(getAdminsList, sort, perPage, currentPage, filter);
            yield put({
                type: 'SET_STATE',
                payload: {
                    list,
                    isLoading: false,
                },
            });
        },
        * GET_ADMIN({payload}, {put, call}) {
            yield put({
                type: 'SET_STATE',
                payload: {
                    isLoadingEntry: true,
                },
            });
            const entry = yield call(getAdmin, payload.id);
            yield put({
                type: 'SET_STATE',
                payload: {
                    entry,
                    isLoadingEntry: false,
                },
            });
        },
        * CREATE_ADMIN({payload}, {call, put}) {
            try {
                yield put({
                    type: 'SET_STATE',
                    payload: {
                        isUpdating: true,
                    },
                });
                const response = yield call(createAdmin, payload);
                yield put({
                    type: 'SET_STATE',
                    payload: {
                        isUpdating: false,
                    },
                });
                yield put(routerRedux.push('/admin/admins'));
                return response;
            } catch (e) {
            }
        },
        * UPDATE_ADMIN({payload}, {call, put}) {
            try {
                yield put({
                    type: 'SET_STATE',
                    payload: {
                        isUpdating: true,
                    },
                });
                const response = yield call(updateAdmin, payload);
                yield put({
                    type: 'SET_STATE',
                    payload: {
                        isUpdating: false,
                    },
                });
                yield put(routerRedux.push('/admin/admins'));
                return response;
            } catch (e) {
            }
        },
        * DELETE_ADMIN({payload}, {call, put}) {
            try {
                yield call(deleteAdmin, payload.id);
            } catch (e) {
                console.log(e);
            }
            yield put({
                type: 'GET_ADMINS',
            });
        },
        editWatcher: getWatcher('/admin/admins/edit/:id', function* ({put}, payload) {
            yield put({
                type: 'GET_ADMIN',
                payload,
            });
        }),
        pageWatcher: getWatcher('/admin/admins', function* ({put, all}) {
            yield all([
                put({
                    type: 'SET_STATE',
                    payload: {
                        entry: {},
                    },
                }),
                put({
                    type: 'GET_ADMINS',
                }),
            ]);
        }),
    },
};
