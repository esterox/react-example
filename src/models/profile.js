import {getWatcher} from 'utils/watcher';

export const Profile = {
    namespace: 'profile',
    state: {},
    reducers: {},
    effects: {
        profileWatcher: getWatcher('/profile', function* ({put, select}) {
            const {isAdmin = false} = yield select(({user}) => user);
            if (!isAdmin) {
                yield put({
                    type: 'retailer/ENSURE',
                });
            }
        }),
    },
    subscriptions: {}
};
