export const Location = {
    namespace: 'location',
    state: {
        pathname: null,
    },
    reducers: {
        CHANGE: (state, {payload}) => ({...state, ...payload}),
    },
    effects: {
        * FILTER({payload}, {put, select}) {
            const {pathname: current} = yield select(({location}) => location);
            if (payload && payload.pathname !== current) {
                yield put({
                    type: 'CHANGE',
                    payload,
                });
            }
        },
        * QUERY({predicate}, {select}) {
            const {pathname} = yield select(({location}) => location);
            return predicate ? predicate(pathname) : pathname;
        },
    },
    subscriptions: {
        setup: ({dispatch, history}) => {
            const handler = (location) => {
                dispatch({
                    type: 'FILTER',
                    payload: location,
                });
            };

            handler(history.location);
            history.listen(handler);
        },
    },
};
