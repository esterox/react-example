import React from 'react';
import { Select } from 'antd';
import { connect } from 'dva';
import userPages from './UserPages';
import { withRouter } from 'dva/router';

const Option = Select.Option;

const mapStateToProps = ({ customer, user }) => ({ customer, user });

@withRouter
@connect(mapStateToProps)
class QualityControlHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCustomerId: null,
      selectedPage: null,
    };

    const { location } = this.props;
    const customerId = localStorage.getItem('selectedCustomerId');
    this.state.selectedPage = location.pathname;
    if (customerId) {
      this.state.selectedCustomerId = customerId;
    }
  }

  handleChangeCustomer = (customerId) => {
    const customerOldId = localStorage.getItem('selectedCustomerId');
    if (!customerId) {
      localStorage.removeItem('selectedCustomerId');
    } else {
      localStorage.setItem('selectedCustomerId', customerId);
      if (customerId !== customerOldId) {
        this.setState({selectedCustomerId: customerId});
        this.getCustomerAndChangeUserProperty();
      }
      this.previewPage();
    }
  };
  handleChangePage = (page) => {
    this.setState({selectedPage: page});
    this.previewPage();
  };

  previewPage = () => {
    setTimeout(() => {
      if (this.state.selectedCustomerId && this.state.selectedPage) {
        const path = `${this.state.selectedPage}?customerId=${this.state.selectedCustomerId}`;
        this.props.history.push(path);
      }
    }, 0);
  };
  getCustomerAndChangeUserProperty = () => {
    const {user, dispatch, location} = this.props;
    if (user.isAdmin) {
      const customerId = localStorage.getItem('selectedCustomerId');
      if (!user.customer || (customerId && user.customer.id !== customerId)) { // important for getting needed customer one time
        dispatch({
          type: 'user/CHANGE_CUSTOMER',
          payload: {}
        }).then((user) => {
          if (location.pathname === '/dashboard') {
            dispatch({
              type: 'dashboard/ALL',
              payload: {},
            }).then((data) => {});
          }
        });
      } else if (user.customer) {
        if (location.pathname === '/dashboard') {
          dispatch({
            type: 'dashboard/ALL',
            payload: {},
          }).then((data) => {});
        }
      }
    }
  };

  componentWillMount() {
    setTimeout(() => {
      this.getCustomerAndChangeUserProperty();
    }, 1000);
  }
  render() {
    const { customer } = this.props;
    const { selectedCustomerId, selectedPage } = this.state;
    return (
        <div className="card">
            <div className="row">
              <div className="col-md-4">
                <div className="utils__title">
                  <strong>Customers</strong>
                </div>
              </div>
              <div className="col-md-4">
                <Select
                    className="mr-3"
                    showSearch
                    allowClear
                    style={{ width: 250 }}
                    placeholder="Select a customer"
                    optionFilterProp="children"
                    value={selectedCustomerId}
                    onChange={this.handleChangeCustomer}
                >
                  {customer.customers && customer.customers.map(option => (option.status === 'active' && <Option value={option.id} key={option.id}>{option.company}</Option>))}
                </Select>
              </div>
              <div className="col-md-4">
                <Select
                    className="mr-3"
                    showSearch
                    allowClear
                    style={{ width: 250 }}
                    placeholder="Select a page"
                    optionFilterProp="children"
                    value={selectedPage}
                    onChange={this.handleChangePage}
                >
                  {userPages.map((option, index) => <Option value={option.pathname} key={index}>{option.title}</Option>)}
                </Select>
              </div>
            </div>
        </div>);
  }
}

export default QualityControlHeader;
