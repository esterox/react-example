import React from 'react';
import QualityControlHeader from "./QualityControlHeader";

export class AdminQualityControlPage extends React.Component {
  render() {
    return (
      <div>
        <div className="card">
          <div className="card-header">
            <QualityControlHeader />
          </div>
        </div>
      </div>
    );
  }
}