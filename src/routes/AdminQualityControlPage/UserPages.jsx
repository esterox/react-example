export default [
    {pathname: '/dashboard', title: 'MainPage', component: 'DashboardPage'},
    {pathname: '/products', title: 'Products', component: 'ProductsPage'},
    {pathname: '/retailers', title: 'Retailers', component: 'RetailersPage'},
    {pathname: '/competitionSku', title: 'CompetitionSku', component: 'CompetitionSkuPage'},
    {pathname: '/placement', title: 'Placement', component: 'PlacementPage'},
    {pathname: '/shelf-dashboard', title: 'ShelfDashboard', component: 'ShelfDashboardPage'},
    {pathname: '/search-results', title: 'SearchResults', component: 'SearchResultPage'},
    {pathname: '/retailer-dashboard', title: 'RetailerDashboard', component: 'RetailerDashboardPage'},
    {pathname: '/reports/pricingReport', title: 'Reports->PricingReport', component: 'ReportPricingPage'},
    {pathname: '/reports/buyBox', title: 'Reports->ByBox', component: 'ReportBuyBoxPage'}
];
