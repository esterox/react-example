import React from 'react'
import {connect} from 'dva'
import {Form, Input, Button} from 'antd'
import {Link} from 'react-router-dom';

const mapStateToProps = (state, props) => ({});

@connect(mapStateToProps)
@Form.create()
export class RestorePasswordForm extends React.Component {

    onSubmit = event => {
        event.preventDefault();
        const {form, isLoading, onSubmit} = this.props
        if (!isLoading) {
            form.validateFields((error, values) => {
                if (!error) {
                    onSubmit(values)
                }
            })
        }
    };

    render() {
        const {form, username} = this.props;
        const {isLoading} = this.props;

        return (
            <div className="cat__pages__login__block__form">
                <h4 className="text-uppercase">
                    <strong>Restore Password</strong>
                </h4>
                <br/>
                <Form layout="vertical" hideRequiredMark onSubmit={this.onSubmit}>
                    <Form.Item label="Username">
                        {form.getFieldDecorator('username', {
                            initialValue: username,
                            rules: [
                                {required: true, message: 'Please input username or email'}
                            ]
                        })(<Input size="default"/>)}
                    </Form.Item>
                    <div className="mb-2">
                        <Link
                            to={'/login'}
                            className="utils__link--blue utils__link--underlined"
                        >
                            Back to login
                        </Link>
                    </div>
                    <div className="form-actions">
                        <Button
                            type="primary"
                            className="width-150 mr-4"
                            htmlType="submit"
                            loading={isLoading}
                        >
                            Restore Password
                        </Button>
                    </div>
                </Form>
            </div>
        )
    }
}
