import React from 'react'
import {Form, Input, Button, message, Row, Col, Select, Spin, notification} from 'antd'
import AvatarEditor from 'react-avatar-editor'
import {connect} from 'dva'

const formOptions = {
    onValuesChange: (props, values) => {
    },
};

const mapStateToProps = ({user, retailer}) => ({user, retailer})

@Form.create(formOptions)
@connect(mapStateToProps)
export class ProfilePage extends React.Component {
    state = {
        avatarPreview: this.props.user.avatar,
        retailers: (function () {
            try {
                return JSON.parse(localStorage['retailers']);
            } catch (e) {
                return ["1", "4"];
            }
        })()
    };

    handleImageChange = e => {
        e.preventDefault();

        function checkFile(file) {
            if (!/^image\/(x-png|bmp|jpg|jpeg|gif|png)$/i.test(file.type)) {
                message.error('You can only upload Image files.');
                return false
            }

            const smallerThenOneMb = file.size / 1024 / 1024 > 1;
            if (smallerThenOneMb) {
                message.error('Image must smaller than 1MB!');
                return false
            }

            return true
        }

        let reader = new FileReader();
        let file = e.target.files[0];

        if (file && checkFile(file)) {
            reader.onloadend = () => {
                this.setState({
                    avatarPreview: reader.result,
                })
            };
            reader.readAsDataURL(file)
        }
    };

    setEditorRef = editor => (this.editor = editor)

    checkPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!')
        } else {
            callback()
        }
    };
    checkConfirm = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], {force: true})
        }
        callback();
    };

    handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({confirmDirty: this.state.confirmDirty || !!value})
    };

    onSubmit = event => {
        event.preventDefault();
        const { form, dispatch } = this.props;
        form.validateFields((error, values) => {
            if (!error) {
                values.avatar = this.editor.getImageScaledToCanvas().toDataURL();

                if (!values.password) {
                    delete values.password;
                    delete values.confirm;
                } else {
                    values.firstLogged = 1;
                }

                dispatch({
                    type: 'user/UPDATE',
                    payload: values,
                }).then(() => {
                    notification.success({
                        message: 'Success',
                        description: 'Profile was successfully updated',
                    });
                    form.setFieldsValue({
                        password: '',
                        confirm: ''
                    })
                })
            }
        })
    };

    saveRetailers = (list) => {
        localStorage["retailers"] = JSON.stringify(list);
        this.setState({
            retailers: list,
        });
    };

    render() {
        const {form, user, retailer} = this.props;
        const {retailers} = this.state;

        return (
            <div>
                {!(user && user.isAdmin) && <div className="card">
                    <div className="card-header">
                        <div className="utils__title">
                            <strong>App Settings</strong>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="utils__form">
                            <Form.Item
                                colon={false}
                                label={
                                    (
                                        <span>
                      <span>Pricing Report Default Retailers - </span>
                      <span className="color__muted">You can select up to 10 retailers from list</span>
                    </span>
                                    )
                                }
                            >
                                <Spin spinning={retailer && retailer.isLoading}>
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Please select default retailers"
                                        mode="tags"
                                        onChange={this.saveRetailers}
                                        value={retailers}
                                        allowClear={true}
                                        filterOption={(query, {props: {children}}) => (children || '').toLowerCase().includes(query.toLowerCase())}
                                        size="large"
                                    >
                                        {retailer && retailer.list.map(({retailerId, retailerName}) => <Select.Option
                                            value={String(retailerId)} key={retailerId}>{retailerName}</Select.Option>)}
                                    </Select>
                                </Spin>
                            </Form.Item>

                        </div>
                    </div>
                </div>}
                <div className="card">
                    <div className="card-header">
                        <div className="utils__title">
                            <strong>User Profile</strong>
                            {(user.firstLogged === 0) && <p className={'text-danger'}>Please change your password.</p>}
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="utils__form">
                            <Form hideRequiredMark onSubmit={this.onSubmit}>
                                <div className="row">
                                    <div className="col-lg-12 offset-lg-0">
                                        <Form.Item label="Company" colon={false}>
                                            <Input size="large" disabled={true}
                                                   value={user.customer && user.customer.company}/>
                                        </Form.Item>
                                        <Form.Item label="Username" colon={false}>
                                            <Input size="large" disabled={true} value={user.login}/>
                                        </Form.Item>
                                        <Form.Item label="Name" colon={false}>
                                            {form.getFieldDecorator('name', {
                                                initialValue: user.name,
                                                rules: [{required: true}],
                                            })(<Input size="large"/>)}
                                        </Form.Item>
                                        <Form.Item label="Phone" colon={false}>
                                            {form.getFieldDecorator('phone', {
                                                initialValue: user.phone,
                                                rules: [{required: true}],
                                            })(<Input size="large"/>)}
                                        </Form.Item>
                                        <Form.Item label="Email" colon={false}>
                                            {form.getFieldDecorator('email', {
                                                initialValue: user.email,
                                                rules: [
                                                    {
                                                        type: 'email',
                                                        message: 'The input is not valid E-mail',
                                                    },
                                                    {
                                                        required: true,
                                                        message: 'Please input your E-mail',
                                                    },
                                                ],
                                            })(<Input size="large"/>)}
                                        </Form.Item>
                                        <Row gutter={20}>
                                            <Col span={12}>
                                                <Form.Item label="New Password" colon={false}>
                                                    {form.getFieldDecorator('password', {
                                                        initialValue: '',
                                                        rules: [
                                                            {
                                                                required: false,
                                                                message: 'Please input your password',
                                                            },
                                                            {
                                                                validator: this.checkConfirm,
                                                            },
                                                            {
                                                                min: 6,
                                                                message: 'Password must be at least 6 characters',
                                                            },
                                                        ],
                                                    })(<Input type="password" size="large"/>)}
                                                </Form.Item>
                                            </Col>
                                            <Col span={12}>
                                                <Form.Item label="Repeat Password" colon={false}>
                                                    {form.getFieldDecorator('confirm', {
                                                        initialValue: '',
                                                        rules: [
                                                            {
                                                                required: false,
                                                                message: 'Please confirm your password',
                                                            },
                                                            {
                                                                validator: this.checkPassword,
                                                            },
                                                        ],
                                                    })(
                                                        <Input type="password" size="large"
                                                               onBlur={this.handleConfirmBlur}/>,
                                                    )}
                                                </Form.Item>
                                            </Col>
                                            {
                                                (user.firstLogged === 0 && !form.getFieldValue('password') && !form.getFieldValue('confirm')) && (
                                                    <Col span={12}>
                                                        <p className={'text-danger'}>Please add new password.</p>
                                                    </Col>
                                                )
                                            }
                                        </Row>
                                        <Form.Item label="Avatar" colon={false}>
                                            <div className="float-left">
                                                <AvatarEditor
                                                    image={this.state.avatarPreview}
                                                    width={50}
                                                    height={50}
                                                    border={10}
                                                    ref={this.setEditorRef}
                                                    color={[0, 0, 0, 0.1]}
                                                />
                                            </div>
                                            <div style={{marginLeft: 80}}>
                                                <Input type="file" size="large"
                                                       onChange={e => this.handleImageChange(e)}/>
                                                <div className="color__muted">
                                                    Please follow the next rules: file can be only JPG format and
                                                    smaller then
                                                    1Mb.
                                                </div>
                                            </div>
                                        </Form.Item>
                                    </div>
                                </div>
                                <div className="form-actions">
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        loading={user.isUpdating}
                                        size="large"
                                    >
                                        Save Profile
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}