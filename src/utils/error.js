import {
    notification
} from 'antd';

export function onError(headers) {
    return (error) => {
        const show = (message, description) => {
            if (!headers || headers.Authorization) {
                notification.warn({
                    message,
                    description
                });
            }
        };

        if (error.response) {
            error.response.json().then(({
                                            name,
                                            message
                                        }) => {
                show(name || 'Authorization error', message);
            }).catch((e) => {
                show('Unhandled error', e.message);
            });
        } else {
            show('Error', error.message);
        }
    };
}