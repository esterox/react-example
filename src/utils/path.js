import pathToRegExp from 'path-to-regexp';

export const params = (pattern, pathname) => {
    const keys = [];
    const regex = pathToRegExp(pattern, keys);
    const match = regex.exec(pathname);

    // return false if case if pattern not matched.
    if (!match) return false;

    // get params
    return keys.reduce((map, param, index) => ({
        ...map,
        [param.name]: match[-~index],
    }), {});
};