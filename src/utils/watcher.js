import {
    params
} from 'utils/path';

export function getWatcher(predicateOrPattern, callback) {
    return [function* (saga) {
        const update = function* () {
            yield saga.put({
                type: 'user/IS_GUEST',
                success: function* ({
                                        pathname
                                    }) {
                    const payload = (predicateOrPattern instanceof Function)
                        ? yield predicateOrPattern()
                        : params(predicateOrPattern, pathname);

                    if (payload) {
                        yield callback(saga, payload, update);
                    }

                    yield saga.take('location/CHANGE');
                    yield update();
                },
                fail: function* () {
                    yield saga.take('user/AUTHORIZED');
                    yield update();
                },
            })
        };

        yield saga.take('user/AUTHORIZED');
        yield update();
    }, {
        type: 'watcher'
    }];
}