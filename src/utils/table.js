import {each, isEmpty, includes, has, map} from 'lodash';
import moment from 'moment';

export function fill(data, getValue = (key, entry) => null) {
    const keys = [];

    // collect all of unique keys
    each(data, (entry) => {
        each(entry, (value, key) => {
            if (!includes(keys, key)) {
                keys.push(key);
            }
        });
    });

    if (!isEmpty(keys)) {
        data = map(data, (entry) => {
            const object = {...entry};
            each(keys, (key) => {
                if (!has(object, key)) {
                    object[key] = getValue(key, object);
                }
            });
            return object;
        });
    }

    return data;
}

export function getKeys(data, predicate = (key) => moment(key, 'YYYY-MM-DD', true).isValid()) {
    const keys = [];

    // collect all of unique keys
    each(data, (entry) => {
        each(entry, (value, key) => {
            if (!includes(keys, key) && predicate(key)) {
                keys.push(key);
            }
        });
    });

    return keys.sort();
}