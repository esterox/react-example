const raf = (fn) => new Promise(resolve => (window.requestAnimationFrame || window.setTimeout)(() => resolve(fn && fn())));
export default raf;