export const toMatrix = (data, rows, cols, predicate, least) => {
    let row = [];
    const matrix = [];

    for (let i = 0; i < rows && data.length; i++) {
        row = data.splice(0, cols);
        matrix.push(predicate ? (row = row.map(predicate)) : row);
    }

    if (row.length && data.length && least) {
        row.pop();
        row.push(least(data.length + 1));
    }

    return matrix;
};