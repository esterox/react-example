import {request} from 'utils/request';

export async function getAdminsList(sort, perPage, currentPage, filter) {
    const filterJSON = JSON.stringify(filter);
    return (await request(`users?sort=${sort}&per-page=${perPage}&currentPage=${currentPage}&filter=${filterJSON}`));
}

export async function getAdmin(id) {
    return (await request(`users/${id}?expand=customer`));
}

export async function createAdmin(data) {
    return await request('users', {
        method: 'POST',
        body: JSON.stringify(data),
    });
}

export async function updateAdmin(data) {
    return await request(`users/${data.id}`, {
        method: 'PUT',
        body: JSON.stringify(data),
    });
}

export async function deleteAdmin(id) {
    return await request(`users/${id}`, {
        method: 'PUT',
        body: JSON.stringify({status: 'deleted'}),
    });
}
