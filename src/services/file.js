import {request} from 'utils/request';

export async function uploadFile(blob, fileName) {
    const formData = new FormData();
    formData.append('file', blob, fileName);

    return await request('file/upload', {
        method: 'POST',
        body: formData,
    });
}