import {request} from 'utils/request';

export async function uploadFile(data) {
    return await request('request', {
        method: 'POST',
        body: JSON.stringify(data),
    });
}