import * as React from 'react';
import { Route as DvaRoute } from 'dva/router';

export const AppRoute = ({ component, layout, ...rest }) => {
  const [Layout, Component] = [layout, component];
  return (
    <DvaRoute
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
};
