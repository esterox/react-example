import React from 'react';
import {BackTop, Icon, Layout as AntLayout, Spin} from 'antd';
import {BaseLayout} from './BaseLayout';
import {enquireScreen, unenquireScreen} from 'enquire-js';
import {ContainerQuery} from 'react-container-query';
import classNames from 'classnames';
import TopBar from 'components/LayoutComponents/TopBar';
import Footer from 'components/LayoutComponents/Footer';

const AntContent = AntLayout.Content;
const AntHeader = AntLayout.Header;
const AntFooter = AntLayout.Footer;
const query = {
    'screen-xs': {
        maxWidth: 575,
    },
    'screen-sm': {
        minWidth: 576,
        maxWidth: 767,
    },
    'screen-md': {
        minWidth: 768,
        maxWidth: 991,
    },
    'screen-lg': {
        minWidth: 992,
        maxWidth: 1199,
    },
    'screen-xl': {
        minWidth: 1200,
        maxWidth: 1599,
    },
    'screen-xxl': {
        minWidth: 1600,
    },
};

export class UserLayout extends BaseLayout {
    state = {
        isMobile: false,
        isLoading: false,
    };

    componentDidMount() {
        this.enquireHandler = enquireScreen(isMobile => this.setState({isMobile}))
    }

    componentWillUnmount() {
        unenquireScreen(this.enquireHandler)
    }

    render() {
        const {children} = this.props
        const {isMobile, isLoading} = this.state

        if (isLoading) {
            return <Spin indicator={<Icon type="loading" style={{fontSize: 24}} spin/>}/>
        }

        return (
            <BaseLayout>
                <ContainerQuery query={query}>
                    {params => (
                        <div className={classNames(params)}>
                            <AntLayout>
                                <BackTop/>
                                <AntLayout>
                                    <AntHeader>
                                        <TopBar/>
                                    </AntHeader>
                                    <AntContent style={{height: '100%'}}>
                                        <div className="utils__content">
                                            {children}
                                        </div>
                                    </AntContent>
                                    <AntFooter>
                                        <Footer/>
                                    </AntFooter>
                                </AntLayout>
                            </AntLayout>
                        </div>
                    )}
                </ContainerQuery>
            </BaseLayout>
        )
    }
}